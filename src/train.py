# Import necessary libraries
import os
import base64
import pandas as pd
from google.cloud import storage
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
import pickle

# Google credentials should be assigned to a service account
# Also note that resources should be configured to use the proper, restricted service account

# Load the data
client = storage.Client()
bucket = client.get_bucket('') # Add bucket name
blob = bucket.blob('') #Add filename
blob.download_to_filename('') # Add filename
data = pd.read_csv('') #Add filename

# Preprocessing
y = data['Class']  
X = data.drop(['Class', 'Time'], axis=1)  

# Scale 'Amount' column
sc = StandardScaler()
X['Amount'] = sc.fit_transform(X['Amount'].values.reshape(-1,1))

# Split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# SMOTE for imbalanced data
sm = SMOTE(random_state=42)
X_train_res, y_train_res = sm.fit_resample(X_train, y_train)

# Model
model = RandomForestClassifier(random_state=42)
model.fit(X_train_res, y_train_res)

# Evaluation
predictions = model.predict(X_test)
print(classification_report(y_test, predictions))

# Save Model
model_filename = "model.pkl"
pickle.dump(model, open(model_filename, 'wb'))

# Upload the model to Google Cloud Storage
storage_client = storage.Client()
bucket = storage_client.get_bucket('') # Add bucket name
blob_model = bucket.blob('models/model.pkl')
blob_model.upload_from_filename('model.pkl')
